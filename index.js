console.log("Hello, world!!!...");
console. log("Hello, batch 203!...");

console. 
log
("Hello, all!...");


// comments:

// This is a single line comment. ctrl + /

/*
This is a multi line comment
ctrl + shift + /

*/

let myVariable = "Hello";

console.log(myVariable);

// console.log(hello);

// let hello;

let productName = 'desktop computer';
console.log(productName)

let product = "Alvin's computer";


let productPrice = "18999";
console.log(productPrice);


const interest = 3.539;



productName = "Laptop";

let friend = "Kate";
friend = "Jane";

console.log(friend);




console.log(interest);

let supplier;

supplier = "John Smith Trading"


supplier = "Zuitt Store";
console.log(supplier);

//const pi;
// pi = 3.1416
//console.log(pi); //error due to const initialization



// a = 5;
// console.log(a);
// var a;


// let	productCode = "DC017";
// const	productBrand = "Dell";
//console.log(productCode, productBrand);


// const	let = "hello";
//console.log(let);


// strings are series of characters that create a word, a phrase, a sentence, or anything related to creating text.

let country = "Phillippines";
let province = "Metro Manila";

// Concatenation Strings
let fullAddress = province + ',' + country;
console.log(fullAddress);


let greeting = "i live in the " + country;
console.log(greeting);

let mailAddress = "Metro manila\nPhilippines";
console.log(mailAddress);


let message = "John's employees went home early.";

console.log(message);

message = 'Johny\'s employees went home early.';
console.log(message);

// Numbers
// Intergers/Whole Numbers
let headcount = 26;
console.log(headcount);


// decimal numbers / fractions
let grade = 98.7;
console.log(grade);



//exponential notation
let planetDistance = 2e10
console.log(planetDistance);


//combine number and strings
console.log("John's grade last quesrter is " + grade);




// Boolean
// True / False

let isMarried = false;
let isGoodConduct = true;
console.log("isMarried" + isMarried);
console.log("isGoodConduct" + isGoodConduct);


//array
//it is used to store multiple values with similar data type
// Syntax:
// let/const arrayName = [elementA,elementB,elementC,..];

let grades = [98.7,94.2, 93.6];
console.log(grade);




let details = ["John","Smith", 32, true];
console.log(details);


let person = {
	fullName: "Juan Dela Cruz",
	age: 35,
	isMarried: false,
	contact: ["+63917 123 4567", "8123 4567"],
	address: {
		houseNumber: "345",
		city: "Manila"
	}
}

console.log(person);

let myGrades = {
	firstGrading: 98.7,
	secondGrading: 92.1,
	fourthGrading: 94.6
}

console.log(typeof myGrades);
console.log(typeof grades);


const anime = ["one piece","one punch man","attack on titan"]
anime[0] = "kimetsu no yaiba"

console.log(anime);




// Null

let spouse = null;
console.log(spouse);

let myNumber = 0;
let myString = "";